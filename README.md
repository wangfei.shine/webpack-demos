# webpack-demo-self

This project is my own webpack examples, referred Runyf's demos
https://github.com/ruanyf/webpack-demos

## Target
1. Seperate dependencies install to each sub-project, make it more clear each plugin can do.
2. Update plugins and loaders to latest version
3. Add more awesome plugins in this demos.

## Sample detail
1. demo1: Simplest webpack demo
2. demo2: Two entry files target
3. demo3: Babel-loader transform React project to javascript
4. demo4: Use css-loader style-loader to import css style file
5. demo5: Url-loader loader image
6. demo6: demo4 union demo5
7. demo7: Use UglifyJsPlugin plugin to optimize js code
8. demo8: Use HtmlWebpackPlugin to generate html index.html
9. demo9: Define custome variable
10. demo10: ??
11. demo11: 