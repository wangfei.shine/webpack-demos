module.exports = {
    entry: {
        app: './main.js',
        vendor: ['jquery']
    },
    output: {
        filename: 'bundle.js'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    name: 'vendor',
                    filename: 'vendor.js',
                    chunks: 'initial',
                    minChunks: 2
                }
            }
        }
    }
}