import { hello } from "./hello";

function showHello(divName: string, name: string): void {
    const element = document.getElementById(divName);
    element.innerHTML = hello(name);
}

showHello('greeting', 'wangfei');