function hello(name: string): string {
    return `Hello from ${name}`;
}

export { hello };