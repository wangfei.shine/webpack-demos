module.exports = {
    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    },
    entry: './main.ts',
    output: {
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.ts[x]?$/,
                exclude: /node_modules/,
                loader: 'awesome-typescript-loader',
                // 即使不需要以下的配置也可以正常编译，并删除.babelrc文件
                // 如果没有以下配置的话，我认为应该是使用了tsc进行编译
                // 注意bable 7要添加babelCore配置
                options: {
                    useBabel: true,
                    babelCore: '@babel/core'
                }
            }
        ]
    }
};