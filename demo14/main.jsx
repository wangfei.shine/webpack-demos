import * as React from 'react'
import * as ReactDOM from 'react-dom';
import { data } from './data';

ReactDOM.render(
    <h1>{data}</h1>,
    document.body
);