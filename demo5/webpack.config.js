module.exports = {
    entry: './main.js',
    output: {
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(png|jpg)$/,
                use: [
                    {
                        loader: 'url-loader',// url-loader需要file-loader，这里可以直接使用file-loader
                        options: {
                            limit: 8192
                        }
                    }
                ]
            }
        ]
    }
}